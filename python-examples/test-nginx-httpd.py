#!/usr/bin/env python3
from sys import argv
from http.server import BaseHTTPRequestHandler, HTTPServer


port = int(argv[1])
text = argv[2].encode("utf8")


class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header("Content-Type", "text/plain")
        self.end_headers()
        self.wfile.write(text)


def run(server_class=HTTPServer, handler_class=SimpleHTTPRequestHandler):
    server_address = ("127.0.0.1", port)
    httpd = server_class(server_address, handler_class)
    print(f"Server running on http://127.0.0.1:{port}/ with \"{text}\"")
    httpd.serve_forever()


run()
