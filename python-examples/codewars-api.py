#!/usr/bin/env python3
import json
import requests
from os import linesep as newline
from sys import argv

MAX_KATAS_PER_PAGE = 200
USERS_API_URL = "https://www.codewars.com/api/v1/users"
KATA_API_URL = "https://www.codewars.com/api/v1/code-challenges"
COMPLETED_KATAS_API_URL = "code-challenges/completed"


def my_dict_to_json(data: dict | list) -> str:
    return json.dumps(data, indent=2)


def get_user_data(user: str) -> dict:
    response = requests.get(f"{USERS_API_URL}/{user}")

    if not response:
        raise Exception("Bad HTTP Response")

    return json.loads(response.content)


def get_kata_data(kata: str) -> dict:
    response = requests.get(f"{KATA_API_URL}/{kata}")

    if not response:
        raise Exception("Bad HTTP Response")

    return json.loads(response.content)


def get_user_katas_page(user: str, page: int) -> dict:
    response = requests.get(f"{USERS_API_URL}/{user}/{COMPLETED_KATAS_API_URL}?page={page}")

    if not response:
        raise Exception("Bad HTTP Response")

    return json.loads(response.content)


def get_user_katas(user: str) -> list:
    current_page = 0
    katas = []

    while True:
        res = get_user_katas_page(user, current_page)

        if res["totalItems"] > 0:
            katas += res["data"]
        else:
            break

        if res["totalItems"] >= MAX_KATAS_PER_PAGE:
            current_page += 1
        else:
            break

    return katas


def get_user_latest_kata(user: str) -> dict:
    """
    used for:
    X-Webhook-Event: code_challenge
    solution_finalized webhook
    """

    res = get_user_katas_page(user, 0)

    if res["totalItems"] <= 0:
        return {}

    return res["data"][0]


def main(args):
    user = args[1]
    katas = args[2:]

    user_data = get_user_data(user)
    # katas_data = [(kata, get_kata_data(kata)) for kata in katas]
    # user_katas = get_user_katas(user)
    user_last = get_user_latest_kata(user)

    print(f"{newline}User({user}) Data:")
    print(my_dict_to_json(user_data))

    # print(f"{newline}User({user}) Katas({len(user_katas)}):")
    # print(my_dict_to_json(user_katas))

    if user_last:
        print(f"{newline}User({user}) Latest Kata({user_last['name']}):")
        print(my_dict_to_json(user_last))

    # for i, (kata, kata_data) in enumerate(katas_data):
    #     print(f"{newline}Kata#{i}({kata}) Data:")
    #     print(my_dict_to_json(kata_data))


if __name__ == "__main__":
    main(argv)
