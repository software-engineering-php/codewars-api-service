<?php

namespace App\Http\Resources\V1;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class HookedUserResource extends JsonResource
{
    public function toArray(Request $request): array
    {
        return [
            'id' => $this['id'],
            'codewarsID' => $this['cw_id'],
            'username' => $this['username'],
            'initialHonor' => $this['initial_honor'],
            'honorDelta' => $this['honor_delta'],
            'rank' => $this['rank'],
        ];
    }
}
