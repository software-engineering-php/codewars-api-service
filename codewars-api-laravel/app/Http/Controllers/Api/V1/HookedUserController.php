<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\AddHookedUserRequest;
use App\Http\Resources\V1\HookedUserCollection;
use App\Http\Resources\V1\HookedUserResource;
use App\Models\HookedUser;
use Illuminate\Support\Facades\Http;

class HookedUserController extends Controller
{
    public function index()
    {
        return new HookedUserCollection(HookedUser::all());
    }

    public function add(AddHookedUserRequest $request)
    {
        $username = $request->username;

        $response = Http::get('https://www.codewars.com/api/v1/users/' . $username);
        if (!$response->successful())
            return [
                'success' => false,
                'message' => 'Failed to access external API',
            ];

        $results = $response->json();
        $cw_id = $results['id'];
        $username = $results['username'];
        $initial_honor = $results['honor'];
        $honor_delta = 0;
        $rank = $results['ranks']['overall']['name'];

        if (HookedUser::where('cw_id', $cw_id)->exists())
            return [
                'success' => false,
                'message' => 'This user already exists',
            ];

        $hookedUser = HookedUser::create([
            'cw_id' => $cw_id,
            'username' => $username,
            'initial_honor' => $initial_honor,
            'honor_delta' => $honor_delta,
            'rank' => $rank,
        ]);

        return [
            'success' => true,
            'user' => new HookedUserResource($hookedUser),
        ];
    }

    public function show(HookedUser $hookedUser)
    {
        return new HookedUserResource($hookedUser);
    }
}
