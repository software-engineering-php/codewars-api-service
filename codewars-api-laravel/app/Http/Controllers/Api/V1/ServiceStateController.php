<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\V1\UpdateServiceStateRequest;
use App\Http\Resources\V1\HookedUserCollection;
use App\Models\HookedUser;
use Illuminate\Support\Facades\Http;

class ServiceStateController extends Controller
{
    public function update(UpdateServiceStateRequest $request)
    {
        $state = $request->state;

        return match ($state) {
            'reset' => $this->resetService(),
            'start' => $this->updateCompetition(true),
            'update' => $this->updateCompetition(false),
            default => [
                'success' => false,
                'message' => 'Unexpected new service state',
            ],
        };
    }

    private function resetService() {
        HookedUser::truncate();
        return [
            'success' => true,
        ];
    }

    private function updateCompetition(bool $start) {
        $failedUsers = [];

        foreach (HookedUser::all() as $user) {
            $response = Http::get('https://www.codewars.com/api/v1/users/' . $user['cw_id']);
            if (!$response->successful()) {
                $failedUsers[] = [
                    'id' => $user['id'],
                    'cw_id' => $user['cw_id'],
                    'username' => $user['username'],
                ];

                continue;
            }

            $results = $response->json();
            $honor = $results['honor'];
            $rank = $results['ranks']['overall']['name'];

            if ($start) {
                $user->update([
                    'initial_honor' => $honor,
                    'honor_delta' => 0,
                    'rank' => $rank,
                ]);
            } else {
                $honor_delta = $honor - $user['initial_honor'];
                if ($honor_delta < 0) {
                    $user->update([
                        'initial_honor' => $honor,
                        'honor_delta' => 0,
                        'rank' => $rank,
                    ]);
                } else {
                    $user->update([
                        'honor_delta' => $honor_delta,
                        'rank' => $rank,
                    ]);
                }
            }
        }

        if (!empty($failedUsers)) {
            return [
                'success' => false,
                'users' => $failedUsers,
            ];
        } else {
            return [
                'success' => true,
                'users' => new HookedUserCollection(HookedUser::all()),
            ];
        }
    }
}
