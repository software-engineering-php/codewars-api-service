<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HookedUser extends Model
{
    use HasFactory;

    protected $fillable = [
        'cw_id',
        'username',
        'initial_honor',
        'honor_delta',
        'rank'
    ];
}
