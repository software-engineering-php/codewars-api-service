<?php

use App\Http\Controllers\Api\V1\HookedUserController;
use App\Http\Controllers\Api\V1\ServiceStateController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'v1', 'namespace' => 'App\Http\Controllers\Api\V1'], function () {
    Route::get('users', [HookedUserController::class, 'index']);
    Route::post('users', [HookedUserController::class, 'add']);
    Route::get('users/{hookedUser}', [HookedUserController::class, 'show']);
    Route::post('service', [ServiceStateController::class, 'update']);
});
