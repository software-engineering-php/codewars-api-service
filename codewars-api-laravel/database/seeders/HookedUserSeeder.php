<?php

namespace Database\Seeders;

use App\Models\HookedUser;
use Illuminate\Database\Seeder;

class HookedUserSeeder extends Seeder
{
    public function run(): void
    {
        HookedUser::factory()->count(10)->create();
    }
}
