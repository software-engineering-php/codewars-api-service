<?php

namespace Database\Factories;

use App\Models\HookedUser;
use Illuminate\Database\Eloquent\Factories\Factory;

class HookedUserFactory extends Factory
{
    protected $model = HookedUser::class;

    public function definition(): array
    {
        return [
            'cw_id' => $this->faker->sha256(),
            'username' => $this->faker->userName(),
            'initial_honor' => $this->faker->numberBetween(0, 1_000_000),
            'honor_delta' => $this->faker->numberBetween(0, 1_000),
            'rank' => $this->faker->numberBetween(1, 8) .  $this->faker->randomElement(['kyu', 'dan']),
        ];
    }
}
