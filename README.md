# Codewars API Service

Project's documentation is [here](https://software-engineering-php.gitlab.io/documentation)
and its repository is [here](https://gitlab.com/software-engineering-php/documentation).
Documentation is written in Russian language and uses
[mdBook](https://github.com/rust-lang/mdBook).

# Endpoints

## Service

### `/api/v1/service` (POST) - delete all users (and all their solved katas)

```json
{
    'state': 'reset'
}
```

## Users

### `/api/v1/users` (GET) - get all users

### `/api/v1/users/{id}` (GET) - get `user[id]` information

### `/api/v1/users` (POST) - add new user to track their solved katas

```json
{
    'username': '<username>'
}
```

If any error occures, the response will be:

```json
{
    'success': false,
    'message': '<error_message>'
}
```

Otherwise:

```json
{
    'success': true,
    'user': '<userData>'  // userData like in /api/v1/users/{id}
}
```
